# Debug utilities for LXC tiny container

* Create container

Create Tiny container as described in README.md file here:
https://gitlab.apertis.org/infrastructure/tiny-image-debug-utils/

* Download binaries to the host
with `git clone https://gitlab.apertis.org/d4s/tiny-debug-utils`
or with downloading and unpacking of the tarball created from GitLab repository
https://gitlab.apertis.org/d4s/tiny-debug-utils

* Add mount entry into `config` file
```
lxc.mount.entry = /home/user/tiny-debug-utils/amd64 var/utils none bind,create=dir 0 0
```
there `/home/user/tiny-debug-utils/amd64` is a path to directory with binaries for host's architecture
prepared on previous step

* Start the container

* Attach to container
Need to re-define shell variables PATH and LD_LIBRARY_PATH to work with prepared binaries:
```
lxc-attach -v LD_LIBRARY_PATH=/var/utils/lib -v PATH=/var/utils/bin:$PATH -n tinyrfs bash
```
this command gives a `bash` shell to the container allowing to use additional binaries.
